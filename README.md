## **Information**
This is the readme for the project. 
The python scripts are at a beginner level as my fluency with Python is still at a very early stages.

# Added information for Devops tasks with Ansible
This contains a playbook structure to install a LAMP Stack and deploy a site on a Server.
This is also using Vagrant 1.4 and ansible 1.5.2
>** Below is the directory structure for the Ansible playbooks.
	
	-- group_vars
	|   |-- all
	|   `-- db
	|-- roles
	|   |-- common
	|   |   |-- files
	|   |   |-- handlers
	|   |   |-- meta
	|   |   |-- tasks
	|   |   |   `-- main.yml
	|   |   |-- templates
	|   |   `-- vars
	|   |-- db
	|   |   |-- files
	|   |   |-- handlers
	|   |   |   `-- main.yml
	|   |   |-- meta
	|   |   |-- tasks
	|   |   |   `-- main.yml
	|   |   |-- templates
	|   |   |   `-- my.cnf.j2
	|   |   `-- vars
	|   `-- webserver
	|       |-- files
	|       |-- handlers
	|       |-- meta
	|       |-- tasks
	|       |   |-- install_httpd.yml
	|       |   `-- main.yml
	|       |-- templates
	|       `-- vars
	|-- site_te.yaml
	|-- vagrant_ansible_inventory_cent_TE
	`-- Vagrantfile
