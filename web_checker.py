#!/usr/bin/env python
import httplib
import sys

def check_webserver(address, port, urlpath):
    #connection creation
    if not urlpath.startswith('/'):
        urlpath = '/' + urlpath
    try:
        conn = httplib.HTTPConnection(address, port)
        print 'HTTP connection created successfully'

        #make reqest
        req = conn.request('GET', urlpath)
        print 'request for %s successful' % urlpath

        #get response
        response = conn.getresponse()
        print 'reponse status: %s' % response.status
    except sock.error, e:
        print 'HTTP connection failed : %s' % e
        return False
    finally:
        conn.close()
        print 'HTTP connection closed successfully'
    if response.status in [200, 301]:
        return True
    else:
        return False

if __name__ == '__main__':
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option("-a", "--address", dest="address", default='localhost',
                      help="ADDRESS for webserver", metavar="ADDRESS")
    parser.add_option("-p", "--port", dest="port", type="int", default=80,
                      help="PORT for webserver", metavar="PORT")
    parser.add_option("-u", "--url", dest="urlpath", default='index.html',
                      help="URLPATH to check", metavar="URLPATH")

    (options, args) = parser.parse_args()
    print 'options: %s, args: %s' % (options, args)
    check = check_webserver(options.address, options.port, options.urlpath)
    print 'check_webserver returned %s' % check
    sys.exit(not check)
